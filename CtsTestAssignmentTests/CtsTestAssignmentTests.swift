//
//  CtsTestAssignmentTests.swift
//  CtsTestAssignmentTests
//
//  Created by Sudipta on 20/07/20.
//  Copyright © 2020 Sudipta. All rights reserved.
//

import XCTest
@testable import CtsTestAssignment

class CtsTestAssignmentTests: XCTestCase {

    override func setUpWithError() throws {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }
    
    /// Testing result
    ///  Test Suite 'Selected tests' passed at 2020-07-20 02:33:24.564.
    ///   Executed 1 test, with 0 failures (0 unexpected) in 0.670 (0.678) seconds
        
        func testApiWithExpectation() {
            XCTAssertEqual(kBaseURL, "https://dl.dropboxusercontent.com/s/2iodh4vg0eortkl/facts.json","Url is not same")
               let expect = expectation(description: "Download should succeed")
               
            Service.shred.getdatafromServer(completion: { (data, title, error) in
                XCTAssertNil(error, "Unexpected error occured: \(String(describing: error?.localizedDescription))")
                       XCTAssertNotNil(data, "No data returned")
                       
                       expect.fulfill()
                   })
               
               waitForExpectations(timeout: 2) { (error) in
                XCTAssertNil(error, "Test timed out. \(String(describing: error?.localizedDescription))")
               }
           }
        func testApiResult() {
                          let data = MockData().getData()
                          guard let string = String(data: data, encoding: String.Encoding.isoLatin1) else { return }
                          guard let properData = string.data(using: .utf8, allowLossyConversion: true) else { return }
                          do {
                              let jsonresult = try JSONDecoder().decode(DataModel.self, from: properData)
                           
                         XCTAssertEqual(jsonresult.title, "About Canada", "Expected About Canada base")
                            XCTAssertEqual(jsonresult.rows.count, 14, "Expected 14 rates")
                              
                              
                          } catch let jsonerror {
                            XCTFail("Unknown format")
                              print(jsonerror)
                          }
            
            
            
             
         }

}

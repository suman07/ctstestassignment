//
//  ItemTableViewCellTest.swift
//  CtsTestAssignmentTests
//
//  Created by Sudipta on 20/07/20.
//  Copyright © 2020 Sudipta. All rights reserved.
//

import XCTest
@testable import CtsTestAssignment
class ItemTableViewCellTest: XCTestCase {
    var sut : ViewController!
    var tableView: UITableView!
    private var dataSource: UITableViewDataSource!
       private var delegate: UITableViewDataSource!
       /// Checking resgister the cell appropriate or not.
       /// - Throws: "The first cell should display" will show
       override func setUpWithError() throws {
           sut = ViewController()
           sut.tableView = UITableView(frame: CGRect(x: 0, y: 0, width: sut.view.frame.width, height: sut.view.frame.height), style: .plain)
           sut.tableView.register(TableViewCell.self, forCellReuseIdentifier: TableViewCell.identifier)
          
             
           sut.tableView.dataSource = dataSource
             let indexPath = IndexPath(row: 0, section: 0)
           let cell = sut.tableView.dequeueReusableCell(withIdentifier: TableViewCell.identifier, for: indexPath) as! TableViewCell
               XCTAssertEqual(cell.labelTitle?.text, nil,
                              "The first cell should display")
       }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testExample() throws {
        // This is an example of a functional test case.
        // Use XCTAssert and related functions to verify your tests produce the correct results.
    }

    func testPerformanceExample() throws {
        // This is an example of a performance test case.
        self.measure {
            // Put the code you want to measure the time of here.
        }
    }

}
